﻿using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using RavenMenuItemsToStructures.CreateStructures.CleanUp;
using RavenMenuItemsToStructures.RavenDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.DataAccess.TreeRepository;

namespace RavenMenuItemsToStructures.Job_AddChildrenToStructures
{
    public class UnOptimizeMenuItems : JobBase
    {
        private IDocumentStore store;

        public UnOptimizeMenuItems(EventHandler<string> logging) : base(logging)
        {
        }

        public UnOptimizeMenuItems()
        {
        }

        private static IDocumentStore SetupDataBaseConnection()
        {
            var db = new RavenDbConnector();
            //db.Connect("https://a.free.gdj.ravendb.cloud", "MichalTesty", "d:\\remote\\free.gdj.client.certificate.pfx");
            db.Connect("http://ubudev:8067", "Dp4Menu");
            var store = db.GetDocumentStore();
            store.Initialize();
            return store;
        }

        public IDocumentSession InitializeSession(IDocumentStore store)
        {
            return store.OpenSession();
        }

        public void Start()
        {
            store = SetupDataBaseConnection();
            new AliasSearchIndex().Execute(store);
            //Step1_DuplicateCommonMenuItems();
            Step2_Cleanup();
        }

        private void Step2_Cleanup()
        {
            var cls = new DeadMenuItems(store);
            cls.Clean();
        }


        private void LogAliasInfo(AliasInfo alias)
        {
            var sb = new StringBuilder();
            sb.AppendLine("AliasInfo:");
            for (int i = 0; i < alias.Paths.Count; i++)
            {
                sb.AppendLine($"{alias.Paths[i]} ({alias.LinkedStructures[i]})");
            }


            OnLogData(sb.ToString());


        }

        private void Step1_DuplicateCommonMenuItems()
        {
            OnLogData("Reading alias info");
            var aliases = GetInfo();

            foreach (var item in aliases)
            {
                LogAliasInfo(item);
                var menuItem = GetMenuItem(item.MenuItemId);
                

                

                for (int i = 1; i < item.LinkedStructures.Count; i++)
                {
                    
                    UpdateStructure(item.LinkedStructures[i], menuItem);
                }

            }
        }

        public IEnumerable<AliasInfo> GetInfo()
        {
            using (var session = InitializeSession(store))
            {
                var structures = session
                    .Query<AliasInfo, AliasSearchIndex>()
                    .Where(x => x.AliasCount>1)
                    //.OfType<Structure>()
                    //.Include(x => x.NodeId)
                    .ToList();
               
                return structures;
            }
        }

        private MenuItem GetMenuItem(string id)
        {
            using (var session = InitializeSession(store))
            {
                var structures = session.Load<MenuItem>(id);
                return structures;
            }
        }

        private string StoreMenuItem(MenuItem menuItem)
        {
            using (var session = InitializeSession(store))
            {
                menuItem.Id = null;
                session.Store(menuItem);
                session.SaveChanges();
            }
            return menuItem.Id;
                
        }

        private Structure UpdateStructure(string structureId, MenuItem menuItem)
        {
            
            var menuItemId = StoreMenuItem(menuItem);
            
            
            using (var session = InitializeSession(store))
            {
                var structure = session.Load<Structure>(structureId);
                OnLogData($"Update {structure.Name}({structureId})    NodeId: {structure.NodeId} -> {menuItemId}");

                structure.NodeId = menuItemId;
                
                session.SaveChanges();

                return structure;
            }
        }




    }
}
