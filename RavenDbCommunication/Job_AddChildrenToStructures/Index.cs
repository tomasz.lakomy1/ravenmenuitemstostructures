﻿using Raven.Client.Documents.Indexes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.DataAccess.TreeRepository;

namespace RavenMenuItemsToStructures.Job_AddChildrenToStructures
{
    class Index
    {
    }

    public class StructurePathByChildren : AbstractIndexCreationTask<Structure>
    {
        public StructurePathByChildren()
        {
            Map = structures => from m in structures
                                let node = LoadDocument<MenuItem>(m.NodeId)
                                let pathid = LoadDocument<Structure>(m.Path)
                                let parents = LoadDocument<Structure>(m.Parents)
                                select new
                                {
                                    Node = m.NodeId,

                                    Name = m.Name,

                                    Path = m.Path.Count > 0 ? pathid.Select(x => x.Name).Aggregate(string.Empty, (current, next) => current == string.Empty ? next : current + "\\" + next) : string.Empty,

                                    PathStructures = m.Path
                                };
        }

        public class SearchableFields
        {
            public string Node { get; set; }

            public string Name { get; set; }

            public string Path { get; set; }

            public List<string> PathStructures { get; set; }
        }
    }


    public class StructuresR : AbstractIndexCreationTask<Structure>
    {
        public class Result
        {
            public IEnumerable<string> Authors { get; set; }
        }

        public StructuresR()
        {
            //Map = posts => from post in posts
            //               select new Result
            //               {
            //                   Authors = Recurse(post, x => x.Comments).Select(x => x.Author)
            //               };
            Map = stuctures => from s in stuctures
                               select new
                               {
                                   sid = s.Id,
                                   s.Name,
                                   xPath = Recurse(s, x => LoadDocument<Structure>(x.Parents[0])).Select(x => x.Name)
                               };


        }
    }


    public class AliasSearchIndex : AbstractIndexCreationTask<Structure, AliasInfo>
    {
        public AliasSearchIndex()
        {
            Map = structures => from m in structures
                                let node = LoadDocument<MenuItem>(m.NodeId)
                                let pathid = LoadDocument<Structure>(m.Path)
                                select new
                                {
                                    MenuItemId = m.NodeId,

                                    Names = m.Name,

                                    Paths = m.Path.Count > 0 ? pathid.Select(x => x.Name).Aggregate(string.Empty, (current, next) => current == string.Empty ? next : current + "\\" + next) + "\\" + m.Name : m.Name,

                                    LinkedStructures = m.Id,

                                    AliasCount = 1
                                };
            Reduce = results => from result in results
                                group result by result.MenuItemId into g
                                select new
                                {
                                    MenuItemId = g.Key,
                                    Names = g.Select(x => x.Names),
                                    Paths = g.Select(x => x.Paths),
                                    LinkedStructures = g.Select(x => x.LinkedStructures),
                                    AliasCount = g.Sum(x => x.AliasCount)
                                };
        }
    }
}
