﻿using Raven.Client.Documents;
using Raven.Client.Documents.Linq;
using Raven.Client.Documents.Session;
using RavenMenuItemsToStructures.RavenDb;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.DataAccess.TreeRepository;

namespace RavenMenuItemsToStructures.Job_AddChildrenToStructures
{
    public class AddChildrenToStructures
    {
        public AddChildrenToStructures(EventHandler<string> logging)
        {
            Logging += logging;
        }

        public AddChildrenToStructures()
        {
        }

        public event EventHandler<string> Logging;

        private void LogData(object sender, string e)
        {
            Logging?.Invoke(sender, e);
        }


        public void Start()
        {
            var store = SetupDataBaseConnection();
            new StructurePathByChildren().Execute(store);
            //new StructuresR().Execute(store);
            Step1_GetAllStructures(InitializeSession(store));
            Step2_GetAllStructuresAndAddParents(InitializeSession(store));
        }

        private void Step1_GetAllStructures()
        {
            throw new NotImplementedException();
        }


        public void Step1_GetAllStructures(IDocumentSession session)
        {
            using (session)
            {
                var structures = session.Query<Structure>(null, "Structures").Select(x => x).ToList();

                foreach (var structure in structures)
                {

                    OnLogData($"{structure.Id} - {structure.Name}");
                    var children = structures.Where(x =>  (x.Path.Any()) && (x.Path.Last() == structure.Id)).Select(x=> x.Id).ToList();

                    structure.Children = children;

                    

                    //var parentStructure = new Structure().FromMenuItem(parent, null);
                    //StoreDocument(parentStructure);

                    //OnLogData($"Searching for children of {parent.Id}: {parent.Path}");
                    //var children = session.Query<MenuItem>(null, "MenuItems").Where(x => x.ParentPath == parent.Path).ToList();
                    //foreach (var child in children)
                    //{
                    //    OnLogData(child.Path);
                    //    var childStructure = new Structure().FromMenuItem(child, parentStructure);
                    //    CalculateNewPath(InitializeSession(), child, childStructure);
                    //}
                }
                session.SaveChanges();
            }

        }
        public void Step2_GetAllStructuresAndAddParents(IDocumentSession session)
        {
            using (session)
            {
                var structures = session.Query<Structure>(null, "Structures").Select(x => x).ToList();

                foreach (var structure in structures)
                {

                    OnLogData($"{structure.Id} - {structure.Name}");
                    var parents = structures.Where(x => (x.Children.Any()) && (x.Children.Contains(structure.Id))).Select(x => x.Id).ToList();

                    structure.Parents = parents;
                }
                session.SaveChanges();
            }

        }


        private static IDocumentStore SetupDataBaseConnection()
        {
            var db = new RavenDbConnector();
            //db.Connect("https://a.free.gdj.ravendb.cloud", "MichalTesty", "d:\\remote\\free.gdj.client.certificate.pfx");
            db.Connect("http://ubudev:8067", "Dp4Menu");
            var store = db.GetDocumentStore();
            return store;
        }


        public IDocumentSession InitializeSession(IDocumentStore store)
        {
            return store.Initialize().OpenSession();
        }

        protected virtual void OnLogData(string e)
        {
            Logging?.Invoke(this, e);
        }
    }
}
