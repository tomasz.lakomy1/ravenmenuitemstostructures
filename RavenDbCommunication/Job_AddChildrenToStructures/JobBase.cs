﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RavenMenuItemsToStructures.Job_AddChildrenToStructures
{
    public class JobBase
    {
        public JobBase(EventHandler<string> logging)
        {
            Logging += logging;
        }

        public JobBase()
        {
        }

        public event EventHandler<string> Logging;

        protected virtual void OnLogData(string e)
        {
            Logging?.Invoke(this, e);
        }
    }
}
