﻿using System;
using System.Linq;
using Raven.Client.Documents.Session;
using Utils.DataAccess.TreeRepository;
using RavenMenuItemsToStructures.ExtensionMethods;
using Raven.Client.Documents;

namespace RavenMenuItemsToStructures
{
    public class Pather
    {
        public event EventHandler<string> Logging;


        public Pather(IDocumentStore store)
        {
            this.store = store;
        }

        protected IDocumentStore store { get; set; }

        public void Start()
        { 
            CalculateNewPath(InitializeSession());
        }

        public void CalculateNewPath(IDocumentSession session)
        {
            using (session)
            {
                var rootParents = session.Query<MenuItem>(null, "MenuItems").Where(x => string.IsNullOrEmpty(x.ParentPath)).ToList();

                foreach (var parent in rootParents)
                {

                    var parentStructure = new Structure().FromMenuItem(parent, null);
                    StoreDocument(parentStructure);
                    
                    OnLogData($"Searching for children of {parent.Id}: {parent.Path}");
                    var children = session.Query<MenuItem>(null, "MenuItems").Where(x => x.ParentPath == parent.Path).ToList();
                    foreach (var child in children)
                    {
                        OnLogData(child.Path);
                        var childStructure = new Structure().FromMenuItem(child, parentStructure);
                        CalculateNewPath(InitializeSession(), child, childStructure);
                    }
                }
            }

        }


        public void CalculateNewPath(IDocumentSession session, MenuItem parent, Structure parentStructure)
        {
            StoreDocument(parentStructure);
            OnLogData($"Searching for children of {parent.Id}: {parent.Path}");
            var children = session.Query<MenuItem>(null, "MenuItems").Where(x => x.ParentPath == parent.Path).ToList();
            
            foreach (var child in children)
            {
                OnLogData($"    Child TreeNode: {child.Id}: {child.Path}");


                var childStructure =  new Structure().FromMenuItem(child, parentStructure);
                OnLogData($"    Child struct: {childStructure.AsString()}");

                CalculateNewPath(InitializeSession(), child, childStructure);

                if (childStructure.Path.Count != child.ParentPath.Split('\\').Length)
                {
                    throw new Exception();
                }
            }

        }

        public void StoreDocument(Structure structure)
        {
            var session = InitializeSession();
            session.Store(structure);
            session.SaveChanges();
        }

        public IDocumentSession InitializeSession()
        {
            return store.Initialize().OpenSession();
        }

        protected virtual void OnLogData(string e)
        {
            Logging?.Invoke(this, e);
        }
    }
}
