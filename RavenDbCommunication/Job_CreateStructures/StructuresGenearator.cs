﻿using Raven.Client.Documents;
using RavenMenuItemsToStructures.CreateStructures.Aliases;
using RavenMenuItemsToStructures.CreateStructures.CleanUp;
using RavenMenuItemsToStructures.RavenDb;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace RavenMenuItemsToStructures.CreateStructures
{
    public class StructuresGenearator
    {
        public StructuresGenearator(EventHandler<string> logging)
        {
            Logging += logging;
        }

        public StructuresGenearator()
        {
        }

        public event EventHandler<string> Logging;

        private void LogData(object sender, string e) 
        {
            Logging?.Invoke(sender, e);
        }

        

        public void Start()
        {

            var store = SetupDataBaseConnection();

            Console.WriteLine("Building structures for menuitems");
            Stetp1_CreateStructures(store);
            Console.WriteLine("Building structures finished. Press any key to continue...");
            Console.ReadKey();

            Console.WriteLine("Reading aliases paths");
            var aliasesLists = Step2_GetAliasesPaths();
            Console.WriteLine("Reading aliases paths finished. Press any key to continue...");
            Console.ReadKey();

            Console.WriteLine("Building aliases.");
            Step3_FindAliases(store, aliasesLists);
            Console.WriteLine("Building aliases finished. Press any key to continue...");
            Console.ReadKey();

            Console.WriteLine("Clean up.");
            Step4_CleanUp(store);
            Console.WriteLine("Clean up finished. Press any key to continue...");
            Console.ReadKey();
        }

        private static IDocumentStore SetupDataBaseConnection()
        {
            var db = new RavenDbConnector();
            //db.Connect("https://a.free.gdj.ravendb.cloud", "MichalTesty", "d:\\remote\\free.gdj.client.certificate.pfx");
            db.Connect("http://ubudev:8067", "Dp4Menu");
            var store = db.GetDocumentStore();
            return store;
        }



        private void Stetp1_CreateStructures(IDocumentStore store)
        {
            var p = new Pather(store);
            p.Logging += LogData;
            p.Start();
        }

        private List<List<string>> Step2_GetAliasesPaths()
        {
            var dup = new OptimalizationLogParser(@"d:\Remote\RavenMenuItemsToStructures\optimalization.log");
            var aliasesLists = dup.GetAliasesLists();
            return aliasesLists;
        }

        private void Step3_FindAliases(IDocumentStore store, List<List<string>> aliasesLists)
        {
            var db = new RavenAliasCorrector(store);
            db.Logging += LogData;
            db.MakeAliases(aliasesLists);
        }

        private void Step4_CleanUp(IDocumentStore store)
        {
            var deadMenuItems = new DeadMenuItems(store);
            deadMenuItems.Logging += LogData;
            deadMenuItems.Clean();
        }

       
    }
}
