﻿using Raven.Client.Documents;
using Raven.Client.Documents.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.DataAccess.TreeRepository;

namespace RavenMenuItemsToStructures.CreateStructures.CleanUp
{
    public class DeadMenuItems
    {
        public event EventHandler<string> Logging;

        public DeadMenuItems(IDocumentStore store)
        {
            this.store = store;
        }

        protected IDocumentStore store { get; set; }

        private List<string> GetUsedMenuItemsFromStructures()
        {
            using (var session = store.OpenSession())
            {
                var structures = session
                    .Query<Structure>()
                    .Select(x => x.NodeId)
                    .OfType<string>()
                    .ToList();

                return structures;
            }
        }

        public void Clean()
        {
            var menuItemsFromStructures = GetUsedMenuItemsFromStructures();

            using (var session = store.OpenSession())
            {
                var items = session
                    .Query<MenuItem>()
                    .Where(x => !x.Id.In(menuItemsFromStructures))
                    .Select(x => new { x.Id, x.Path })
                    .ToList();


                //}
                //using (var session = store.OpenSession())
                //{
                foreach (var item in items)
                {
                    OnLogging($"{item.Id}: {item.Path}");
                    //OnLogging($"{item.}");
                    session.Delete(item.Id);
                }
                session.SaveChanges();
            }

        }

        protected virtual void OnLogging(string e)
        {
            Logging?.Invoke(this, e);
        }
    }
}
