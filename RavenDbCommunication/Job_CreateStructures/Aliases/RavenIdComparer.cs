﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace RavenMenuItemsToStructures.Aliases
{
    class RavenIdComparer : IComparer<string>
    {
        public int Compare([AllowNull] string x, [AllowNull] string y)
        {
            var splitx = x.Split("/");
            var idx = int.Parse(splitx[1].Remove(splitx[1].Length - 2));

            var splity = y.Split("/");
            var idy = int.Parse(splity[1].Remove(splity[1].Length - 2));

            if (idx > idy) return 1;
            else if (idx == idy) return 0;
            else return -1;
        }
    }


}