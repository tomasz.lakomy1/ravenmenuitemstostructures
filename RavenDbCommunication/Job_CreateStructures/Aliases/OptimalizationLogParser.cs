﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace RavenMenuItemsToStructures.CreateStructures.Aliases
{
    public class OptimalizationLogParser
    {

        private List<List<string>> paths = new List<List<string>>();
        private readonly string logFilePath;
        public OptimalizationLogParser(string logFilePath)
        {
            this.logFilePath = logFilePath;
            ReadLogData();
        }

        private void ReadLogData()
        {
            var log = File.ReadAllLines(logFilePath);

            for (int i = 0; i < log.Length; i++)
            {
                var line = log[i];

                if (Regex.Match(line, "HASH:").Success)
                {
                    i = ReadPaths(i + 2, log);
                }

            }

        }

        private int ReadPaths(int idx, string[] log)
        {
            var list = new List<string>();
            while (log[idx] != "")
            {
                list.Add(log[idx]);
                idx++;
            }

            paths.Add(list);
            return idx;
        }

        public List<List<string>> GetAliasesLists()
        {
            return paths;
        }
    }
}
