﻿using MenuGenerator.Models;
using Raven.Client.Documents;
using Raven.Client.Documents.Conventions;
using Raven.Client.Documents.Linq;
using Raven.Client.Documents.Operations;
using RavenMenuItemsToStructures.Aliases;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Utils.DataAccess.TreeRepository;

namespace RavenMenuItemsToStructures.CreateStructures.Aliases
{
    public class RavenAliasCorrector
    {
        public event EventHandler<string> Logging;

        public RavenAliasCorrector(IDocumentStore store)
        {
            this.store = store;

            new StructureFullPath().Execute(store);
        }

        protected IDocumentStore store { get; set; }

        public void MakeAliases(List<List<string>> aliasesPathsList)
        {
            foreach (var paths in aliasesPathsList)
            {
                MakeAlias(paths);
            }

        }
        /// <summary>
        /// W tej wersji nie tworzymy aliasów dla directoryItemów tylko dla pozycji programów
        /// </summary>
        /// <param name="paths"></param>
        public void MakeAlias(List<string> paths)
        {

            OnLogging($"Aliases for: {Environment.NewLine + "    "}{string.Join(Environment.NewLine + "    ", paths)}");
            var aliases = new Dictionary<string, List<StructureFullPath.SearchableFields>>();
            using (var session = store.OpenSession())
            {
                // pobieram dane pozycji mających być aliasami
                var structures = session
                    .Query<StructureFullPath.SearchableFields, StructureFullPath>()
                    .Where(x => x.Path.In(paths))
                    .ProjectInto<StructureFullPath.SearchableFields>()
                    .ToList();


                // dodaje te pozycje do slownika z kluczem "" bo to poczatek w strukturze drzewa

                foreach (var data in structures)
                {
                    if (data.Item.Data.GetType() != typeof(DirectoryItem))
                    {
                        aliases.Add("", structures);
                    }

                }




                foreach (var path in paths)
                {
                    // wyszukuje wszystkie podfoldery i pozycje zawarte w pozycjach "rodzicach" pobranych wyzej
                    var path1 = path + "\\";
                    var childStructures = session
                    .Query<StructureFullPath.SearchableFields, StructureFullPath>()
                    .Where(x => x.Path.StartsWith(path1) && x.Path != path)
                    .ProjectInto<StructureFullPath.SearchableFields>()
                    .ToList();

                    // usuwam poczatkowa czesc sciezki "rodzica" zostawiajac tylko nazwe pozycji lub podfolder i nazwe pozycji jesli wystepuje
                    // nastepnie dodaje to jako klucz w slowniku
                    var dict = childStructures.ToDictionary(x => $"{x.Path.Substring(path.Length + 1)}", y => y);

                    // sumuje slownik stworzony wyzej i moj glowny slownik ze wszystkim elementami ktore maja byc aliasami
                    foreach (var pos in dict)
                    {
                        if (pos.Value.Item.Data.GetType() != typeof(DirectoryItem))
                        {
                            if (aliases.ContainsKey(pos.Key))
                            {
                                aliases[pos.Key].Add(pos.Value);
                            }
                            else
                            {
                                aliases.Add(pos.Key, new List<StructureFullPath.SearchableFields> { pos.Value });
                            }
                        }

                    }

                    //var childPaths = childStructures.Select(x => $"{x.Path.Substring(path.Length+1)} - {x.Structure.NodeId}");


                    OnLogging(string.Join("," + Environment.NewLine, childStructures.Select(x => $"\"{x.Path}\"").ToArray()));
                    //OnLogging(string.Join($",{ Environment.NewLine}", childPaths.Select(x=> $"\"{x}\"")));


                }
            }

            foreach (var alias in aliases)
            {
                using (var session = store.OpenSession())
                {

                    // bazujac na slowniku z wszyskimi aliasami, dla kazdego klucza tworze kolekcje menuitem.id 
                    // elementów ktore nalezy polaczyc w jeden
                    var nodeId = new SortedSet<string>(alias.Value.Select(x => x.Node), new RavenIdComparer());

                    // nastepnie pobieram structure.id elementow ktore te menuitemy zawieraja
                    var structToChange = alias.Value.Select(x => x.Id);

                    OnLogging($"Changing strctures: {string.Join(", ", structToChange.Select(x => "\"" + x + "\""))}{Environment.NewLine + "    "}Setting NodeId = {nodeId.Min}");

                    // podmieniam we wszystkich strukturach NodeId na jeden (o najniższym indeksie)
                    var s = session.Load<Structure>(structToChange).Values;

                    foreach (var structure in s)
                    {
                        structure.NodeId = nodeId.Min;
                    }
                    session.SaveChanges();
                }

            }
        }

        protected virtual void OnLogging(string e)
        {
            Logging?.Invoke(this, e);
        }
    }

}