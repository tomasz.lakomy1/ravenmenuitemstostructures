﻿using Raven.Client.Documents.Indexes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Utils.DataAccess.TreeRepository;

namespace RavenMenuItemsToStructures.Aliases
{
    public class StructureFullPath : AbstractIndexCreationTask<Structure, StructureFullPath.SearchableFields>
    {
        public class SearchableFields
        {
            public string Id { get; set; }
            public string Node { get; set; }

            public string Name { get; set; }

            public string Path { get; set; }

            public Structure Structure { get; set; }

            public MenuItem Item { get; set; }

        }

        public StructureFullPath()
        {
            Map =  Structures => from m in Structures
                                let node = LoadDocument<MenuItem>(m.NodeId)
                                let pathid = LoadDocument<Structure>(m.Path)

                                select new
                                {
                                    Id = m.Id,

                                    Node = m.NodeId,

                                    Name = m.Name,

                                    Path = m.Path.Count > 0 ? pathid.Select(x => x.Name).Aggregate("", (current, next) => current == "" ? next : current + "\\" + next) + "\\" + m.Name : m.Name,
                                    
                                    Structure = m,

                                    Item = node
                                };
            Store(x => x.Node, FieldStorage.Yes);
            Store(x => x.Name, FieldStorage.Yes);
            Store(x => x.Path, FieldStorage.Yes);
            Store(x => x.Structure, FieldStorage.Yes);
            Store(x => x.Item, FieldStorage.Yes);
        }
    }
}
