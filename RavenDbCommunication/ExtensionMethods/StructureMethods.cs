﻿using System;
using System.Collections.Generic;
using System.Text;
using Utils.DataAccess.TreeRepository;

namespace RavenMenuItemsToStructures.ExtensionMethods
{
    public static class StructureMethods
    {
        public static Structure FromMenuItem(this Structure obj, MenuItem currentNode, Structure parentStructure)
        {
            var structure = obj;
            structure.Path = new List<string>();

            structure.NodeId = currentNode?.Id;
            structure.Name = currentNode?.Data.Name;

            if (parentStructure != null)
            {
                structure.Path.AddRange(parentStructure.Path);
                structure.Path.Add(parentStructure.Id);
            }

            return structure;
        }

        public static string AsString(this Structure obj)
        {
            var structure = obj;
            var paths = String.Join(",", structure.Path.ToArray());

            var text = $"Id= {structure.Id}, NodeId= {structure.NodeId}, Name= {structure.Name} Paths[]= {paths}";

            return text;
        }
    }

   
}
