﻿using Raven.Client.Documents;
using Raven.Client.Documents.Conventions;
using Raven.Client.Documents.Operations;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace RavenMenuItemsToStructures.RavenDb
{
    public class RavenDbConnector
    {
        protected IDocumentStore store { get; set; }
        public void Connect(string serverUrl, string dataBaseName, string certificatePath)
        {            
            X509Certificate2 clientCertificate = new X509Certificate2(certificatePath);

            store = new DocumentStore()
            {
                Urls = new[] { serverUrl },
                Conventions = new DocumentConventions { OperationStatusFetchMode = OperationStatusFetchMode.Polling },
                Certificate = clientCertificate,
                Database = dataBaseName
            };

            store.OnBeforeQuery += (sender, beforeQueryExecutedArgs) =>
            {
                beforeQueryExecutedArgs.QueryCustomization.WaitForNonStaleResults();
            };
            store.Initialize();                        
        }

        public void Connect(string serverUrl, string dataBaseName)
        {
            
            store = new DocumentStore()
            {
                Urls = new[] { serverUrl },
                Conventions = new DocumentConventions { OperationStatusFetchMode = OperationStatusFetchMode.Polling },            
                Database = dataBaseName
            };

            store.OnBeforeQuery += (sender, beforeQueryExecutedArgs) =>
            {
                beforeQueryExecutedArgs.QueryCustomization.WaitForNonStaleResults();
            };
            store.Initialize();
        }

        public IDocumentStore GetDocumentStore()
        {
            return store;
        }
    }
}
