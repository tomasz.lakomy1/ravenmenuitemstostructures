﻿using Raven.Client.Documents;
using RavenMenuItemsToStructures.Aliases;
using RavenMenuItemsToStructures.CleanUp;
using RavenMenuItemsToStructures.CreateStructures;
using RavenMenuItemsToStructures.Job_AddChildrenToStructures;
using RavenMenuItemsToStructures.RavenDb;
using System;
using System.Collections.Generic;
using System.IO;

namespace RavenMenuItemsToStructures
{
    class Program
    {

        static void Main(string[] args)
        {
            //var createStructures = new StructuresGenearator(LogData).Start();

            new AddChildrenToStructures(LogData).Start(); // dodawanie parent - child do structures
            //new UnOptimizeMenuItems(LogData).Start();
        }

     

        private static void LogData(object sender, string e)
        {
            LogToConsole(e);
            LogToFile(e);
        }



        private static void LogToConsole(string text)
        {
            Console.WriteLine(text);
        }

        private static void LogToFile(string text)
        {
            File.AppendAllText("program.log", text + Environment.NewLine);
        }
    }
}
