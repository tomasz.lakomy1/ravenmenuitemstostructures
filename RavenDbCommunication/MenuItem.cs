﻿using MenuGenerator.Models;

namespace RavenMenuItemsToStructures
{
    public class MenuItem
    {
        public string Id { get; set; }

        public string Path { get; set; }
        public string ParentPath { get; set; }

        //public Structure Structure { get; set; }
        public DirectoryItem Data { get; set; }

        //public string IdBasedPath { get; set; }

    }
}
